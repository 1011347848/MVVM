package com.weicong.mvvm;

import android.content.Context;
import android.text.Editable;
import android.view.View;
import android.widget.Toast;

public class MyHandlers {

    private Context mContext;

    private User mUser;

    public MyHandlers(Context context, User user) {
        mContext = context;
        mUser = user;
    }

    public void onClickButton(View view) {
        Toast.makeText(mContext, "Click Button", Toast.LENGTH_SHORT).show();
    }

    public void afterFirstNameChanged(Editable s) {
        mUser.setFirstName(s.toString());
    }
}
