package com.weicong.mvvm;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.weicong.mvvm.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_main);
        User user = new User("Fan", "Weicong");
        MyHandlers handlers = new MyHandlers(this, user);
        binding.setUser(user);
        binding.setHandlers(handlers);
    }
}
