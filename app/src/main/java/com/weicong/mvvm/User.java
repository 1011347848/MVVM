package com.weicong.mvvm;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

public class User extends BaseObservable {
    /*
    直接声明为public，不需要getter方法
    public String firstName;
    public String lastName;
    */

    //声明为private，需要getter方法
    private String firstName;
    private String lastName;

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Bindable
    public String getFirstName() {
        return firstName;
    }

    @Bindable
    public String getLastName() {
        return lastName;
    }

    /*
    除了上面的getXXX形式，也可以定义如下的属性名和方法名相同的方法
    public String firstName() {
        return firstName;
    }

    public String lastName() {
        return lastName;
    }
     */



    /*
     代码修改对象属性时通知界面更新：
     第一种方案：
     1. 让类继承BaseObservable
     2. 需要通知的属性的get方法上加上@Bindable
     3. 调用notifyPropertyChanged方法通知界面刷新
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(com.weicong.mvvm.BR.firstName);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        notifyPropertyChanged(com.weicong.mvvm.BR.lastName);
    }

    /*
    第二种方案：
    使用ObservableField类
    public ObservableField<String> firstName = new ObservableField<>();
    public ObservableField<String> lastName = new ObservableField<>();
     */
}
